type TupleToObject<T extends readonly string[], U> = {
    [K in T[number]]: U
}

const tuple = ['classic', 'peach breeze', 'pig \'N egg', 'bitburger'] as const;

type ObjectFromTuple = TupleToObject<typeof tuple, number>
// expected to be { classic: number, 'peach breeze': number, 'pig \'N egg': number, bitburger: number }

export { }