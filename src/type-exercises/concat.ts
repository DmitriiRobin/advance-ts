type Concat<T extends any[], U extends any[]> = {
    (arr1: T, arr2: U): [...T, ...U]
}

type Result1 = Concat<[1], [2]>; // expected to be [1, 2]
type Result2 = Concat<['classic'], ['peach breeze', 'bitburger']>; // expected to be ['classic', 'peach breeze', 'bitburger']
type Result3 = Concat<[], [3]>; // expected to be [3]

export { }