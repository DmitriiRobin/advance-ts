type CustomOmit<T extends object, L> = {
    [Property in keyof T as Exclude<Property, L>]: T[Property]
}

interface Burger {
    name: string;
    numberOfSlices: number;
    meat: string;
}

type VeganBurger = CustomOmit<Burger, 'numberOfSlices' | 'meat'> // expected to be { name: string }

export { }