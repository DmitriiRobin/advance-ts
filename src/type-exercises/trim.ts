type Trim<T> = T extends `  ${infer N}  ` ? N : T

type Trimmed = Trim<'  Hello World  '> // expected to be 'Hello World'

export { }