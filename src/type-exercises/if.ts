type If<F extends boolean, S, T> = F extends true ? S : T

type Classic = If<true, 'classic', 'bitburger'>;  // expected to be 'classic'
type Bitburger = If<false, 'classic', 'bitburger'>; // expected to be 'bitburger'

export { }